import { Component } from '@angular/core';
import { APP_NAME, APP_ROUTES } from './shared/constants';
import { MatDialog } from '@angular/material/dialog';
import { LoginModalComponent } from '@features/login-modal/login-modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  public title: string = APP_NAME;
  public routes = APP_ROUTES;

  constructor(public dialog: MatDialog) {}

  openLoginModal(): void {
    const dialogRef = this.dialog.open(LoginModalComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }
}
